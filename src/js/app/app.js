(function () {
    var JSONP_GET_IP_URL = "http://ip-api.com/json/";

    var $form = $('#form'),
        $messageInput = $('.message').children('textarea'),
        $themeInput = $('.theme').children('input'),
        $buttons = $('.buttons'),
        $submitBtn = $buttons.children('.submit-message'),
        $cancelBtn = $buttons.children('.cancel'),
        $insertSmileyBtn = $('.emoticon'),
        $insertIpBtn = $('.insert-ip');

    //Блокировка/разблокировка кнопок "Отправить" и "Отмена"
    function changeButtonsDisabled () {
        var messageInputLength = $messageInput.val().length,
            themeInputLength = $themeInput.val().length;
        if (messageInputLength && themeInputLength) {
            setEnabled($submitBtn);
        }
        else {
            setDisabled($submitBtn);
        }
        if (messageInputLength || themeInputLength) {
            setEnabled($cancelBtn);
        }
        else {
            setDisabled($cancelBtn);
        }

        function setDisabled ($btn) {
            $btn.removeClass('enabled').addClass('disabled');
            $btn.attr('tabindex', -1);
        }
        function setEnabled ($btn) {
            $btn.removeClass('disabled').addClass('enabled');
            $btn.attr('tabindex', 1);
        }
    }

    //Очистка формы
    function clearForm () {
        if ($cancelBtn.hasClass('enabled')) {
            $messageInput.add($themeInput).val('');
            changeButtonsDisabled();
            resizeTextarea();
        }
    }

    //Отправка формы
    function submitForm () {
        if ($submitBtn.hasClass('enabled')) {
            alert("Тема: " + $themeInput.val() + "; Сообщение: " + $messageInput.val());
            clearForm();
        }
    }

    $messageInput.add($themeInput).on('input', changeButtonsDisabled);

    $cancelBtn.on('click', function () {
        clearForm();
        return false;
    });

    $submitBtn.on('click', function () {
        submitForm();
        return false;
    });

    //Для перехода по табуляции с последнего элемента на первый, и наоборот
    $('.focusguard').on('focus', function () {
        if ($(this).hasClass('fg-begin')) {
            $form.find('[tabindex=1]').eq(-2).focus();
        }
        else if ($(this).hasClass('fg-end')) {
            $form.find('[tabindex=1]').eq(1).focus();
        }
    });

    //установка действий по нажатию Enter/Ctrl+Enter в поле ввода сообщения
    $messageInput.on('keydown', function (e) {
        if (e.keyCode == 13) {
            if (e.ctrlKey) {
                insertTextInTextarea("\n", this);
            }
            else {
                submitForm();
            }
            return false;
        }
    });

    //вставка ip по нажатию кнопки
    $insertIpBtn.on('click', function () {
        $.ajax({
            "url": JSONP_GET_IP_URL,
            dataType: "jsonp",
            success: function (data) {
                insertTextInTextarea(data.query, $messageInput.get(0));
                changeButtonsDisabled();
            }
        });
    });

    //работа со списком смайликов
    (function () {
        var $content = $(document.getElementById('smilies-template').innerHTML);
        $content.children('i').on('click', function () {
            insertTextInTextarea($(this).data('text'), $messageInput.get(0));
        });
        $insertSmileyBtn.tooltipster({
            theme: 'tooltipster-theme',
            content: $content,
            trigger: "click",
            interactive: true
        });
    }());

    //работа с полем ввода сообщения
    $messageInput.on('change', resizeTextarea);
    $messageInput.on('cut paste drop keydown', function () {
        window.setTimeout(resizeTextarea, 0);
    });

    //изменение размеров поля ввода сообщения
    function resizeTextarea () {
        $messageInput.height('auto');
        $messageInput.height($messageInput.prop('scrollHeight'));
    }

    /**
     * @param {string} text
     * @param {HTMLElement} element
     */
    function insertTextInTextarea (text, element) {
        var val = element.value,
            start = element.selectionStart;
        var end = val.slice(element.selectionEnd);
        element.value = val.slice(0, start) + text + end;
        element.selectionStart = element.selectionEnd = element.selectionEnd - end.length;
        element.focus();
        resizeTextarea();
    }

    (function () {
        $themeInput.focus();
        changeButtonsDisabled();
        resizeTextarea();
    }());
}());