"use strict";
var gulp = require('gulp'),
    jade = require('gulp-jade'),
    less = require('gulp-less'),
    csso = require('gulp-csso'),
    uglify = require('gulp-uglify'),
    rimraf = require('rimraf');

var dest_dir = 'dist/',
    src_dir = 'src/',
    path = {
        build: {
            html: dest_dir,
            css: dest_dir + 'css/',
            js: dest_dir + 'js/'
        },
        src: {
            html: src_dir + 'jade/index.jade',
            less: src_dir + 'less/style.less',
            css: src_dir + 'css/*.css',
            appJs: src_dir + 'js/app/*.js',
            libJs: src_dir + 'js/lib/*.js'
        },
        clean: dest_dir
    };

gulp.task('html:build', function() {
    gulp.src(path.src.html)
        .pipe(jade())
        .pipe(gulp.dest(path.build.html));
});

gulp.task('css:build', function() {
    gulp.src(path.src.less)
        .pipe(less())
        .pipe(csso())
        .pipe(gulp.dest(path.build.css));
    gulp.src(path.src.css)
        .pipe(csso())
        .pipe(gulp.dest(path.build.css));
});

gulp.task('js:build', function() {
    gulp.src(path.src.libJs)
        .pipe(gulp.dest(path.build.js));
    gulp.src(path.src.appJs)
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['clean'], function() {
    gulp.start('html:build', 'css:build', 'js:build');
});